<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
	/*
	 * dans un contrôleur, l'injection de dépendances se fait dans les méthodes
	 */
	public function index(Request $request):Response
	{
		/*
		 * request : permet de récupérer la requête http
		 *    $_GET : $request->query->get(variable)
		 *    $_POST : $request->request->get(variable)
		 *    $_FILES : $request->files->get(variable)
		 */
		/*
		 * dump: équivalent à var_dump
		 * dd: dump et die
		 */
		//dd($request);

		$response = new Response('{"data": {"result":"ok"}}', 404, [
			//'Content-Type' => 'application/json',
			//'X-Message' => 'coucou'
		]);
		return $response;
	}
}
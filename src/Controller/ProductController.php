<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
	/**
	 * @Route("/products/{slug}", name="product.details")
	 */
	public function details(ProductRepository $productRepository, string $slug, CommentRepository $commentRepository):Response
	{
		// sélection d'un produit par son id
		//$result = $productRepository->find($id);

		// formulaire des commentaires
		$type = CommentType::class;
		$form = $this->createForm($type);

		// sélection d'un produit par sa version normalisée
		$result = $productRepository->findOneBy([
			'slug' => $slug
		]);

		// récupération des commentaires existants
		$comments = $commentRepository->getCommentsByProductId( $result->getId() )->getResult();

		return  $this->render('product/details.html.twig', [
			'result' => $result,
			'form' => $form->createView(),
			'comments' => $comments
		]);
	}

	/**
	 * @Route("/products", name="products.index")
	 */
	public function index(ProductRepository $productRepository):Response
	{
		/*
		 * fonctionnement doctrine
		 *   - deux branches
		 *      - gérer les entités (UPDATE, INSERT, DELETE) : EntityManagerInterface
		 *      - classe de dépôt - Repository -  SELECT
		 *          4 méthodes de sélection par défaut
		 *              - findAll : sélectionner toutes les entités
		 *                  SELECT * FROM ...
		 *              - find($id) : sélectionner d'une entité par sa clé primaire
		 *
		 *              - findBy : sélectionner plusieurs entités avec des conditions, limitation des résultats et décalage des résultats
		 *                  SELECT * FROM ... WHERE ... OFFSET.. LIMIT ...
		 *              - findOneBy : sélectionner une entité avec des conditions
		 *                  SELECT * FROM WHERE id=... AND ...
		 */

		// sélection de tous les produits
		$results = $productRepository->findAll();

		return $this->render('product/index.html.twig', [
			'results' => $results
		]);
	}

	/**
	 * @Route("/products/{productId}/comment", name="products.comment", methods={"POST"})
	 */
	public function comment(int $productId, Request $request, ProductRepository $productRepository, EntityManagerInterface $entityManager, CommentRepository $commentRepository):JsonResponse
	{
		// test d'une requête ajax
		if(!$request->isXmlHttpRequest()){
			return new JsonResponse([
				'message' => 'Unauthorized'
			], JsonResponse::HTTP_FORBIDDEN);
		}

		// récupération du contenu de la requête
		$formData = json_decode($request->getContent());

		// création d'un objet Comment
		$comment = (new Comment())
			->setAuthor( $formData->author )
			->setDate( new \DateTime() )
			->setMessage( $formData->message )
			->setProduct( $productRepository->find($formData->productId) )
		;

		$entityManager->persist($comment);
		$entityManager->flush();

		$response = [
			'data' => $commentRepository
				->getCommentsByProductId( $formData->productId )->getArrayResult()
		];

		return new JsonResponse($response);
	}
}






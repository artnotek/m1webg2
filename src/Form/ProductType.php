<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use App\EventSubscriber\Form\ProductFormSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
            	'constraints' => [
            		new NotBlank([
            			'message' => "Le nom est obligatoire"
		            ])
	            ]
            ])
            ->add('description', TextareaType::class, [
	            'constraints' => [
		            new NotBlank([
			            'message' => "La description est obligatoire"
		            ])
	            ]
            ])
            ->add('price', MoneyType::class, [
            	'constraints' => [
            		new NotBlank([
            			'message' => "Le prix est obligatoire"
		            ]),
		            new Positive([
		            	'message' => "Le prix doit être supérieur à 0"
		            ])
	            ]
            ])
            ->add('categories', EntityType::class, [
            	/*
            	 * EntityType : champ de formulaire relié à une entité
            	 *  - class : entité en relation
            	 *  - choice_label : choix d'une propriété de l'entité à afficher
            	 *  - multiple : sélection de plusieurs choix; par défaut false
            	 *  - expanded : affichage de plusieurs champs; par défaut false
            	 *      multiple : false > expanded : false = select
            	 *      multiple : false > expanded : true = boutons radio
            	 *      multiple : true > expanded : true = cases à cocher
            	 *      multiple : true > expanded : false = menu
            	 *  - contraintes pour les cases à cocher
            	 *      Count: comptage du nombre de sélection
            	 */
            	'class' => Category::class,
	            'choice_label' => 'name',
	            'multiple' => true,
	            'expanded' => true,
	            'constraints' => [
	            	new Count([
	            		'min' => 1,
			            'minMessage' => "Vous devez sélectionner une catégorie au minimum"
		            ])
	            ]
            ])
        ;

        // ajout d'un souscripteur de formulaire
	    $builder->addEventSubscriber(new ProductFormSubscriber());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
